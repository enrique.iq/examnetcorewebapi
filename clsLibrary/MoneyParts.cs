﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace clsLibrary
{
    public class MoneyParts
    {
        public List<List<double>> Build(string moneda)
        {
            var arrMoneda = new double[] { 0.05, 0.1, 0.2, 0.5, 1, 2, 5, 10, 20, 50, 100, 200 };
            var result = new List<List<double>>();

            double vMoneda;
            int Indice = 0;
            double.TryParse(moneda, out vMoneda);

            //calcular dimensión
            for (int i = 0; i < arrMoneda.Length; i++)
            {
                if (vMoneda == arrMoneda[i])
                {
                    Indice = i;
                    break;
                }
            }
            //actualizar el tamaño del array
            Array.Resize(ref arrMoneda, Indice + 1);

            foreach (var c in arrMoneda)
            {
                var arr = new List<double>();
                if (vMoneda % c == 0)
                {
                    int n = 0;
                    var cantidad = vMoneda / c;
                    do
                    {
                        arr.Add(c); n++;
                    } while (n < cantidad);
                }
                else
                {
                    var item = arrMoneda.Where(a => a + c == vMoneda).ToList();
                    if (item.Any())
                    {
                        arr.Add(c);
                        foreach (var d in item)
                        {
                            arr.Add(d);
                        }
                    }
                }
                if (arr.Any())
                    result.Add(arr);

            }
            return result;
        }
    }
}
