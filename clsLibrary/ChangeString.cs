﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace clsLibrary
{
    public class ChangeString
    {
        public string Build(string str)
        {
            string result = null;
            string[] abc = { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "ñ", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z" };
            bool isLower = false;
            foreach (char c in str)
            {
                string word;
                if (abc.Contains(c.ToString().ToLower()))
                {
                    isLower = char.IsLower(c);
                    int index = Array.IndexOf(abc, c.ToString().ToLower());
                    if (char.ToLower(c).ToString() == "z")
                        word = "a";
                    else
                        word = abc[index + 1];
                }
                else
                    word = c.ToString();

                if (!isLower)
                    word = word.ToString().ToUpper();
                result += word;
            }
            return result;
        }
    }
}
