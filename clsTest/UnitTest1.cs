using System;
using System.Collections.Generic;
using clsLibrary;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace clsTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestProblema01()
        {
            ChangeString cs = new ChangeString();
            string expected = "123 bcde*3";
            string actual = "123 abcd*3";
            //Assert  
            Assert.AreEqual(expected, cs.Build(actual));
        }

        [TestMethod]
        public void TestProblema02()
        {
            OrderRange or = new OrderRange();
            int[][] expected = new int[2][] { new int[] { 1, 5 }, new int[] { 2, 4 } };
            int[] actual = new int[4] { 2, 1, 4, 5 };

            int[][] result = or.Build(actual);
            var x = ArraysEqual(expected, result);

            Console.WriteLine(x);

        }

        [TestMethod]
        public void TestProblema03()
        {
            MoneyParts mp = new MoneyParts();
            List<List<double>> expected = new List<List<double>>() {
                new List<double> { 0.05,0.05 },new List<double> {0.1 }
            };

            var result = mp.Build("0.1");

            ReferenceEquals(result, expected);


        }

        private static bool ArraysEqual(int[][] a1, int[][] a2)
        {
            if (a1.Length == a2.Length)
            {
                for (int i = 0; i < a1.Length; i++)
                {
                    for (int j = 0; j < a1[i].Length; j++)
                    {
                        if (a1[i][j] != a2[i][j])
                        {
                            return false;
                        }
                    }

                }
                return true;
            }
            return false;
        }
    }
}
