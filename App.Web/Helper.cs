﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace App.Web
{
        public class ServerAPI
        {
            private string _url = "http://localhost:53582";
            public HttpClient InitializeClient()
            {
                var client = new HttpClient();
                client.BaseAddress = new Uri(_url);
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                return client;
            }
        }
}
