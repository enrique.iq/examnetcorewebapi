﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Net.Http;
using App.Web.Models;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Options;

namespace App.Web.Controllers
{
    [Authorize]
    public class SucursalController : Controller
    {
        private readonly ServerAPI _api;
        public SucursalController(IOptions<ServerAPI> api)
        {
            _api = api.Value;
        }
        public async Task<ActionResult> Index()
        {
            List<Sucursal> lst = new List<Sucursal>();
            try
            {
                HttpClient client = _api.InitializeClient();
                HttpResponseMessage resposne = await client.GetAsync("api/sucursal");
                if (resposne.IsSuccessStatusCode)
                {
                    var obj = resposne.Content.ReadAsStringAsync().Result;
                    lst = JsonConvert.DeserializeObject<List<Sucursal>>(obj);
                }
            }
            catch (Exception)
            {

                return View();
            }


            return View(lst);
        }
        public async Task<ActionResult> Details(int id)
        {
            var result = new Sucursal();
            try
            {
                HttpClient client = _api.InitializeClient();
                HttpResponseMessage resposne = await client.GetAsync($"api/sucursal/{id}");
                if (resposne.IsSuccessStatusCode)
                {
                    var obj = resposne.Content.ReadAsStringAsync().Result;
                    result = JsonConvert.DeserializeObject<Sucursal>(obj);
                }
                return View(result);
            }
            catch
            {
                return RedirectToAction("Error");
            }

        }
        public async Task<ActionResult> Create()
        {
            List<Banco> lst = new List<Banco>();
            try
            {
                HttpClient client = _api.InitializeClient();
                HttpResponseMessage resposne = await client.GetAsync("api/banco");
                if (resposne.IsSuccessStatusCode)
                {
                    var obj = resposne.Content.ReadAsStringAsync().Result;
                    lst = JsonConvert.DeserializeObject<List<Banco>>(obj);
                    var items = lst.Select(x => new SelectListItem() { Text = x.Nombre, Value = x.BancoId.ToString() }).ToList();
                    ViewBag.lstBanco = items;
                }
            }
            catch (Exception)
            {

                return View();
            }

            return View();
        }
        [HttpPost]
        public async Task<ActionResult> Create([Bind(include: "BancoId,SucursalId,Nombre,Direccion")]Sucursal obj)
        {
            try
            {
                HttpClient client = _api.InitializeClient();
                HttpResponseMessage response = await client.PostAsJsonAsync("api/sucursal", obj);
                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
            }
            catch
            {
                return RedirectToAction("Error");
            }

            return RedirectToAction("Error");
        }

        public async Task<ActionResult> Edit(int id)
        {

            var result = new Sucursal();
            try
            {
                List<Banco> lst = new List<Banco>();
                HttpClient client = _api.InitializeClient();
                HttpResponseMessage resposneBanco = await client.GetAsync("api/banco");
                if (resposneBanco.IsSuccessStatusCode)
                {
                    var obj = resposneBanco.Content.ReadAsStringAsync().Result;
                    lst = JsonConvert.DeserializeObject<List<Banco>>(obj);
                    var items = lst.Select(x => new SelectListItem() { Text = x.Nombre, Value = x.BancoId.ToString() }).ToList();
                    ViewBag.lstBanco = items;
                }


                HttpResponseMessage resposne = await client.GetAsync($"api/sucursal/{id}");
                if (resposne.IsSuccessStatusCode)
                {
                    var obj = resposne.Content.ReadAsStringAsync().Result;
                    result = JsonConvert.DeserializeObject<Sucursal>(obj);
                }
                return View(result);
            }
            catch
            {
                return RedirectToAction("Error");
            }
        }

        //The PUT Method
        [HttpPost]
        public async Task<ActionResult> Edit(int id, Sucursal obj)
        {
            try
            {
                HttpClient client = _api.InitializeClient();
                HttpResponseMessage responseMessage = await client.PutAsJsonAsync($"api/sucursal/{id}", obj);
                if (responseMessage.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
                return RedirectToAction("Error");
            }
            catch
            {
                return RedirectToAction("Error");
            }

        }

        public async Task<ActionResult> Delete(int id)
        {
            try
            {
                HttpClient client = _api.InitializeClient();
                HttpResponseMessage responseMessage = await client.DeleteAsync($"api/sucursal/{id}");
                if (responseMessage.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
                return RedirectToAction("Error");
            }
            catch (Exception)
            {

                return RedirectToAction("Error");
            }

        }


    }
}
