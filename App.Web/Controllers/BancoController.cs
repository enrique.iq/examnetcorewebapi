﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using App.Web.Models;
using System.Net.Http;
using Newtonsoft.Json;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Authorization;



// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace App.Web.Controllers
{
    [Authorize]
    public class BancoController : Controller
    {

        private readonly ServerAPI _api;
        public BancoController(IOptions<ServerAPI>   api)
        {
            _api = api.Value;
        }

        // GET: api/Banco
        [HttpGet]
        public async Task<ActionResult> Index()
        {
            List<Banco> lst = new List<Banco>();
            try
            {
                HttpClient client = _api.InitializeClient();

                HttpResponseMessage resposne = await client.GetAsync("api/banco");
                if (resposne.IsSuccessStatusCode)
                {
                    var obj = resposne.Content.ReadAsStringAsync().Result;
                    lst = JsonConvert.DeserializeObject<List<Banco>>(obj);
                }
            }
            catch (Exception)
            {

                return View();
            }


            return View(lst);
        }
        public async Task<ActionResult> Details(int id)
        {
            var result = new Banco();
            try
            {
                HttpClient client = _api.InitializeClient();
                HttpResponseMessage resposne = await client.GetAsync($"api/banco/{id}");
                if (resposne.IsSuccessStatusCode)
                {
                    var obj = resposne.Content.ReadAsStringAsync().Result;
                    result = JsonConvert.DeserializeObject<Banco>(obj);
                }
                return View(result);
            }
            catch
            {
                return RedirectToAction("Error");
            }

        }
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public async Task<ActionResult> Create([Bind(include: "BancoId,Nombre,Direccion")]Banco banco)
        {
            try
            {
                HttpClient client = _api.InitializeClient();
                HttpResponseMessage response = await client.PostAsJsonAsync("api/banco", banco);
                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
            }
            catch
            {
                return RedirectToAction("Error");
            }

            return RedirectToAction("Error");
        }

        public async Task<ActionResult> Edit(int id)
        {

            var result = new Banco();
            try
            {
                HttpClient client = _api.InitializeClient();
                HttpResponseMessage resposne = await client.GetAsync($"api/banco/{id}");
                if (resposne.IsSuccessStatusCode)
                {
                    var obj = resposne.Content.ReadAsStringAsync().Result;
                    result = JsonConvert.DeserializeObject<Banco>(obj);
                }
                return View(result);
            }
            catch
            {
                return RedirectToAction("Error");
            }
        }

        //The PUT Method
        [HttpPost]
        public async Task<ActionResult> Edit(int id, Banco banco)
        {
            try
            {
                HttpClient client = _api.InitializeClient();
                HttpResponseMessage responseMessage = await client.PutAsJsonAsync($"api/banco/{id}", banco);
                if (responseMessage.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
                return RedirectToAction("Error");
            }
            catch
            {
                return RedirectToAction("Error");
            }

        }

        public async Task<ActionResult> Delete(int id)
        {
            try
            {
                HttpClient client = _api.InitializeClient();
                HttpResponseMessage responseMessage = await client.DeleteAsync($"api/banco/{id}");
                if (responseMessage.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
                return RedirectToAction("Error");
            }
            catch (Exception)
            {

                return RedirectToAction("Error");
            }

        }     

    }
}
