﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Net.Http;
using App.Web.Models;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Options;


// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace App.Web.Controllers
{
    [Authorize]
    public class OrdenPagoController : Controller
    {
        private readonly ServerAPI _api;
        public OrdenPagoController(IOptions<ServerAPI> api)
        {
            _api = api.Value;
        }
        public async Task<ActionResult> Index()
        {
            List<OrdenPago> lst = new List<OrdenPago>();
            try
            {
                HttpClient client = _api.InitializeClient();

                HttpResponseMessage resposne = await client.GetAsync("api/ordenpago");
                if (resposne.IsSuccessStatusCode)
                {
                    var obj = resposne.Content.ReadAsStringAsync().Result;
                    lst = JsonConvert.DeserializeObject<List<OrdenPago>>(obj);
                }
            }
            catch (Exception)
            {

                return View();
            }


            return View(lst);
        }
        public async Task<ActionResult> Details(int id)
        {
            var result = new OrdenPago();
            try
            {
                HttpClient client = _api.InitializeClient();

                HttpResponseMessage resposne = await client.GetAsync($"api/ordenpago/{id}");
                if (resposne.IsSuccessStatusCode)
                {
                    var obj = resposne.Content.ReadAsStringAsync().Result;
                    result = JsonConvert.DeserializeObject<OrdenPago>(obj);
                }
                return View(result);
            }
            catch
            {
                return RedirectToAction("Error");
            }

        }
        public async Task<ActionResult> Create()
        {
            List<Sucursal> lst = new List<Sucursal>();
            try
            {
                HttpClient client = _api.InitializeClient();

                HttpResponseMessage resposne = await client.GetAsync("api/sucursal");
                if (resposne.IsSuccessStatusCode)
                {
                    var obj = resposne.Content.ReadAsStringAsync().Result;
                    lst = JsonConvert.DeserializeObject<List<Sucursal>>(obj);
                    var items = lst.Select(x => new SelectListItem() { Text = x.Nombre, Value = x.SucursalId.ToString() }).ToList();
                    ViewBag.lstSucursal = items;
                }
            }
            catch (Exception)
            {

                return View();
            }

            return View();
        }
        [HttpPost]
        public async Task<ActionResult> Create([Bind(include: "SucursalId,Monto,EstadoOrdenPagoId,MonedaId,FechaPago")]OrdenPago obj)
        {
            try
            {
                HttpClient client = _api.InitializeClient();

                HttpResponseMessage response = await client.PostAsJsonAsync("api/ordenpago", obj);
                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
            }
            catch
            {
                return RedirectToAction("Error");
            }

            return RedirectToAction("Error");
        }

        public async Task<ActionResult> Edit(int id)
        {

            var result = new OrdenPago();
            try
            {
                HttpClient client = _api.InitializeClient();

                List<Sucursal> lst = new List<Sucursal>();
                HttpResponseMessage resposneBanco = await client.GetAsync("api/sucursal");
                if (resposneBanco.IsSuccessStatusCode)
                {
                    var obj = resposneBanco.Content.ReadAsStringAsync().Result;
                    lst = JsonConvert.DeserializeObject<List<Sucursal>>(obj);
                    var items = lst.Select(x => new SelectListItem() { Text = x.Nombre, Value = x.SucursalId.ToString() }).ToList();
                    ViewBag.lstSucursal = items;
                }


                HttpResponseMessage resposne = await client.GetAsync($"api/ordenpago/{id}");
                if (resposne.IsSuccessStatusCode)
                {
                    var obj = resposne.Content.ReadAsStringAsync().Result;
                    result = JsonConvert.DeserializeObject<OrdenPago>(obj);
                }
                return View(result);
            }
            catch
            {
                return RedirectToAction("Error");
            }
        }

        //The PUT Method
        [HttpPost]
        public async Task<ActionResult> Edit(int id, OrdenPago obj)
        {
            try
            {
                HttpClient client = _api.InitializeClient();

                HttpResponseMessage responseMessage = await client.PutAsJsonAsync($"api/ordenpago/{id}", obj);
                if (responseMessage.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
                return RedirectToAction("Error");
            }
            catch
            {
                return RedirectToAction("Error");
            }

        }

        public async Task<ActionResult> Delete(int id)
        {
            try
            {
                HttpClient client = _api.InitializeClient();

                HttpResponseMessage responseMessage = await client.DeleteAsync($"api/ordenpago/{id}");
                if (responseMessage.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
                return RedirectToAction("Error");
            }
            catch (Exception)
            {

                return RedirectToAction("Error");
            }

        }
    }
}
