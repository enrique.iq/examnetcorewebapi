﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace App.Web.Models
{
    public class OrdenPago : EntityBase
    {
        [DisplayName("Código")]
        public int? OrdenPagoId { get; set; }
        [DisplayName("Código sucursal")]
        public int? SucursalId { get; set; }
        [Required(ErrorMessage = "El campo es requerido")]
        public decimal? Monto { get; set; }
        [DisplayName("Moneda")]
        public int? MonedaId { get; set; }
        [Required(ErrorMessage = "El campo es requerido")]
        public DateTime? FechaPago { get; set; }
        [DisplayName("Estado órden de pago")]
        public int? EstadoOrdenPagoId { get; set; }
        public virtual EstadoOrdenPago EstadoOrdenPago { get; set; }
        public virtual Sucursal Sucursal { get; set; }
        public virtual Moneda Moneda { get; set; }
    }
}
