﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace App.Web.Models
{
    public class Moneda
    {
        [DisplayName("Código Moneda")]
        public int MonedaId { get; set; }
        [DisplayName("Nombre moneda")]
        [Required(ErrorMessage = "El campo es requerido")]
        public string Nombre { get; set; }
    }
}
