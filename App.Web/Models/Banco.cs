﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace App.Web.Models
{
    public class Banco : EntityBase
    {
        public Banco() {
            Sucursal = new HashSet<Sucursal>();
        }
        [DisplayName("Código")]
        public int? BancoId { get; set; }
        [DisplayName("Descripción")]
        [Required(ErrorMessage = "El campo es requerido")]
        public string Nombre { get; set; }
        [DisplayName("Dirección")]
        [Required(ErrorMessage = "El campo es requerido")]
        public string Direccion { get; set; }
        public virtual ICollection<Sucursal> Sucursal { get; set; }
    }
}
