﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace App.Web.Models
{
    [Table("AppUser")]
    public class AppUser
    {
        [Key]
        public int UserId { get; set; }
        [Required(ErrorMessage ="El campo es requerido")]
        public string FirstName { get; set; }
        [Required(ErrorMessage = "El campo es requerido")]
        public string lastName { get; set; }
        [Required(ErrorMessage = "El campo es requerido")]
        public string Email { get; set; }
        [Required(ErrorMessage = "El campo es requerido")]
        public string UserName { get; set; }
        [Required(ErrorMessage = "El campo es requerido")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        [Compare("Password",ErrorMessage ="La contraseña no son iguales")]
        public string ConfirmPassword { get; set; }
    }
}
