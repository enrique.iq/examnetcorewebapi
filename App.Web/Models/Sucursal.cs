﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace App.Web.Models
{
    public class Sucursal:EntityBase
    {
        public Sucursal()
        {
            OrdenPago = new HashSet<OrdenPago>();
        }
        [DisplayName("Código")]
        public int SucursalId { get; set; }
        [DisplayName("Nombre banco")]
        public int BancoId { get; set; }
        [DisplayName("Nombre sucursal")]
        public string Nombre { get; set; }
        [DisplayName("Dirección sucursal")]
        [Required(ErrorMessage = "El campo es requerido")]
        public string Direccion { get; set; }
        public virtual Banco Banco { get; set; }        
        public virtual ICollection<OrdenPago> OrdenPago { get; set; }

    }
}
