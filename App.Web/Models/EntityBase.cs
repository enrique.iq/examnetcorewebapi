﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace App.Web.Models
{
    public class EntityBase
    {
        [DisplayName("Creado por")]
        public string CreatedBy { get; set; }
        [DisplayName("Fecha creación")]
        public DateTime? CreatedOn { get; set; }
        [DisplayName("Modificado por")]
        public string ModifiedBy { get; set; }
        [DisplayName("Fecha modificación")]
        public DateTime? ModifiedOn { get; set; }
        public bool Estado { get; set; }
    }
}
