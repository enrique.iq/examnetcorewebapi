﻿using App.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App.API.DataProvider
{
    public interface IOrdenPagoDAO
    {
        Task<IEnumerable<OrdenPago>> Get();
        Task<OrdenPago> GetById(int id);
        Task<int> Add(OrdenPago obj);
        Task<int> Update(OrdenPago obj);
        Task<int> Delete(int id);
        Task<IEnumerable<OrdenPago>> GetOrdenPagoBySucursal(int SucursalId, string monedaId);
    }
}
