﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using App.API.Configuration;
using App.API.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;

namespace App.API.DataProvider
{
    public class BancoDAO : IBancoDAO
    {
        private readonly ConnectionStrings _cnx;

        public BancoDAO(IOptions<ConnectionStrings> options)
        {
            _cnx = options.Value;
        }
        public async Task<int> Add(Banco banco)
        {
            int result = 0;
            try
            {
                using (var sqlConnection = new SqlConnection(_cnx.DbConnection))
                {
                    banco.CreatedBy = "einca";
                    await sqlConnection.OpenAsync().ConfigureAwait(false);
                    using (var cmd = sqlConnection.CreateCommand())
                    {
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.CommandText = "AGREGAR_BANCO";
                        cmd.Parameters.AddWithValue("@Nombre", banco.Nombre);
                        cmd.Parameters.AddWithValue("@Direccion", banco.Direccion);
                        cmd.Parameters.AddWithValue("@Usuario", banco.CreatedBy);
                        await cmd.ExecuteNonQueryAsync().ConfigureAwait(false);
                        result = 1;
                    }
                }
            }
            catch (Exception ex)
            {
                result = 0;
                throw ex;
            }
            return result;
        }
        public async Task<int> Delete(int id)
        {
            int result = 0;
            try
            {
                using (var sqlConnection = new SqlConnection(_cnx.DbConnection))
                {
                    await sqlConnection.OpenAsync().ConfigureAwait(false);
                    using (var cmd = sqlConnection.CreateCommand())
                    {
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.CommandText = "ELIMINAR_BANCO";
                        cmd.Parameters.AddWithValue("@BancoId", id);
                        await cmd.ExecuteNonQueryAsync().ConfigureAwait(false);
                        result = 1;
                    }
                }
            }
            catch (Exception ex)
            {
                result = 0;
                throw ex;
            }
            return result;
        }
        public async Task<IEnumerable<Banco>> Get()
        {

            List<Banco> lst = new List<Banco>();
            try
            {
                
                using (SqlConnection sqlConnection = new SqlConnection(_cnx.DbConnection))
                {
                    await sqlConnection.OpenAsync().ConfigureAwait(false);
                    using (var cmd = sqlConnection.CreateCommand())
                    {
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.CommandText = "LISTAR_BANCO";
                        var _reader = await cmd.ExecuteReaderAsync().ConfigureAwait(false);
                        while (await _reader.ReadAsync().ConfigureAwait(false))
                        {
                            lst.Add(new Banco
                            {
                                BancoId = Convert.ToInt32(_reader["BancoId"]),
                                Nombre = Convert.ToString(_reader["Nombre"]),
                                Direccion = Convert.ToString(_reader["Direccion"]),
                                CreatedBy = Convert.ToString(_reader["CreatedBy"]),
                                CreatedOn = Convert.ToDateTime(_reader["CreatedOn"])
                            });
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                lst = null;
                throw ex;
            }
            return lst.ToList();
        }
        public async Task<Banco> GetById(int id)
        {
            var obj = new Banco();
            try
            {
                using (SqlConnection sqlConnection = new SqlConnection(_cnx.DbConnection))
                {
                    await sqlConnection.OpenAsync().ConfigureAwait(false);
                    using (var cmd = sqlConnection.CreateCommand())
                    {
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.CommandText = "OBTENER_BANCO";
                        cmd.Parameters.AddWithValue("@BancoId", id);
                        var _reader = await cmd.ExecuteReaderAsync().ConfigureAwait(false);
                        while (await _reader.ReadAsync().ConfigureAwait(false))
                        {
                            obj = new Banco
                            {
                                BancoId = Convert.ToInt32(_reader["BancoId"]),
                                Nombre = Convert.ToString(_reader["Nombre"]),
                                Direccion = Convert.ToString(_reader["Direccion"]),
                                CreatedBy = Convert.ToString(_reader["CreatedBy"]),
                                CreatedOn = Convert.ToDateTime(_reader["CreatedOn"])
                            };
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                obj = null;
                throw ex;
            }
            return obj;
        }
        public async Task<int> Update(Banco banco)
        {
            int result = 0;
            try
            {
                using (var sqlConnection = new SqlConnection(_cnx.DbConnection))
                {
                    await sqlConnection.OpenAsync().ConfigureAwait(false);
                    using (var cmd = sqlConnection.CreateCommand())
                    {
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.CommandText = "ACTUALIZAR_BANCO";
                        cmd.Parameters.AddWithValue("@BancoId", banco.BancoId);
                        cmd.Parameters.AddWithValue("@Nombre", banco.Nombre);
                        cmd.Parameters.AddWithValue("@Direccion", banco.Direccion);
                        cmd.Parameters.AddWithValue("@Usuario", banco.CreatedBy);
                        await cmd.ExecuteNonQueryAsync().ConfigureAwait(false);
                        result = 1;
                    }
                }
            }
            catch (Exception ex)
            {
                result = 0;
                throw ex;
               
            }
            return result;
        }
    }
}
