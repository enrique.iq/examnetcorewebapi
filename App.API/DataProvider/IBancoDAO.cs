﻿using App.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App.API.DataProvider
{
   public interface IBancoDAO
    {
        Task<IEnumerable<Banco>> Get();
        Task<Banco> GetById(int id);
        Task<int> Add(Banco banco);
        Task<int> Update(Banco banco);
        Task<int> Delete(int id);

    }
}
