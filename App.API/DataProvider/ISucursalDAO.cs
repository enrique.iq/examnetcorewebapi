﻿using App.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App.API.DataProvider
{
    public interface ISucursalDAO
    {
        Task<IEnumerable<Sucursal>> Get();
        Task<Sucursal> GetById(int id);
        Task<int> Add(Sucursal banco);
        Task<int> Update(Sucursal banco);
        Task<int> Delete(int id);
        Task<IEnumerable<Sucursal>> GetSucursalByBanco(int bancoId);

    }
}
