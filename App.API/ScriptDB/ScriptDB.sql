USE master;
GO



IF EXISTS(SELECT * FROM  sys.databases where name = N'PARTE02')
BEGIN
 ALTER DATABASE PARTE02 SET SINGLE_USER WITH ROLLBACK IMMEDIATE;
 DROP DATABASE PARTE02;
END
GO

CREATE DATABASE PARTE02;
GO

USE  PARTE02;
GO


/*DROP TABLES*/

IF EXISTS (SELECT name FROM sys.tables WHERE name = N'OrdenPago')
   BEGIN     
      DROP TABLE OrdenPago
END 
GO

IF EXISTS (SELECT name FROM sys.tables WHERE name = N'EstadoOrdenPago')
   BEGIN     
      DROP TABLE EstadoOrdenPago
END 
GO


IF EXISTS (SELECT name FROM sys.tables WHERE name = N'Sucursal')
   BEGIN     
      DROP TABLE Sucursal
END 
GO


IF EXISTS (SELECT name FROM sys.tables WHERE name = N'Banco')
   BEGIN     
      DROP TABLE Banco
END 
GO

IF EXISTS (SELECT name FROM sys.tables WHERE name = N'AspNetUserTokens')
   BEGIN     
      DROP TABLE AspNetUserTokens
END 
GO

IF EXISTS (SELECT name FROM sys.tables WHERE name = N'AspNetUsers')
   BEGIN     
      DROP TABLE AspNetUsers
END 
GO

IF EXISTS (SELECT name FROM sys.tables WHERE name = N'AspNetUserRoles')
   BEGIN     
      DROP TABLE AspNetUserRoles
END 
GO

IF EXISTS (SELECT name FROM sys.tables WHERE name = N'AspNetUserLogins')
   BEGIN     
      DROP TABLE AspNetUserLogins
END 
GO

IF EXISTS (SELECT name FROM sys.tables WHERE name = N'AspNetUserClaims')
   BEGIN     
      DROP TABLE AspNetUserClaims
END 
GO

IF EXISTS (SELECT name FROM sys.tables WHERE name = N'AspNetRoles')
   BEGIN     
      DROP TABLE AspNetRoles
END 
GO

IF EXISTS (SELECT name FROM sys.tables WHERE name = N'AspNetRoleClaims')
   BEGIN     
      DROP TABLE AspNetRoleClaims
END 
GO

/*CREATE TABLES*/

CREATE TABLE Banco(
BancoId INT IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
Nombre VARCHAR(60) NOT NULL,
Direccion VARCHAR(256) NOT NULL,
CreatedBy VARCHAR(20) NOT NULL,
CreatedOn DATETIME NOT NULL CONSTRAINT DF_Banco_CreatedOn DEFAULT (GETDATE()),
ModifiedBy VARCHAR(20) NULL,
ModifiedOn DATETIME NULL, 
Estado BIT NOT NULL CONSTRAINT DF_Banco_Estado DEFAULT (1)
);


CREATE TABLE Sucursal(
SucursalId INT IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
BancoId INT NOT NULL,
Nombre VARCHAR(60) NOT NULL,
Direccion VARCHAR(256) NOT NULL,
CreatedBy VARCHAR(20) NOT NULL,
CreatedOn DATETIME NOT NULL CONSTRAINT DF_Sucursal_CreatedOn DEFAULT (GETDATE()),
ModifiedBy VARCHAR(20) NULL,
ModifiedOn DATETIME NULL, 
Estado BIT NOT NULL CONSTRAINT DF_Sucursal_Estado DEFAULT (1)
);


CREATE TABLE OrdenPago(
OrdenPagoId INT IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
SucursalId INT NOT NULL,
Monto NUMERIC(18,2) NOT NULL CONSTRAINT DF_OrdenPago_Monto DEFAULT(0.00),
Moneda INT NOT NULL,
FechaPago DATETIME NULL,
EstadoOrdenPagoId INT NOT NULL CONSTRAINT DF_OrdenPago_EstadoOrdenPagoId DEFAULT (1),
CreatedBy VARCHAR(20) NOT NULL,
CreatedOn DATETIME NOT NULL CONSTRAINT DF_OrdenPago_CreatedOn DEFAULT (GETDATE()),
ModifiedBy VARCHAR(20) NULL,
ModifiedOn DATETIME NULL, 
Estado BIT NOT NULL CONSTRAINT DF_OrdenPago_Estado DEFAULT (1)
);


CREATE TABLE EstadoOrdenPago(
EstadoOrdenPagoId INT IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
Nombre VARCHAR(40) NOT NULL,
CreatedBy VARCHAR(20) NOT NULL,
CreatedOn DATETIME NOT NULL CONSTRAINT DF_EstadoOrdenPago_CreatedOn DEFAULT (GETDATE()),
ModifiedBy VARCHAR(20) NULL,
ModifiedOn DATETIME NULL, 
Estado BIT NOT NULL CONSTRAINT DF_EstadoOrdenPago_Estado DEFAULT (1)
);

CREATE TABLE AspNetRoleClaims(
	Id int IDENTITY(1,1) NOT NULL,
	ClaimType nvarchar(max) NULL,
	ClaimValue nvarchar(max) NULL,
	RoleId nvarchar(450) NOT NULL,
 CONSTRAINT PK_AspNetRoleClaims PRIMARY KEY CLUSTERED (Id ASC)
);

CREATE TABLE AspNetRoles(
	Id nvarchar(450) NOT NULL,
	ConcurrencyStamp nvarchar(max) NULL,
	Name nvarchar(256) NULL,
	NormalizedName nvarchar(256) NULL,
 CONSTRAINT PK_AspNetRoles PRIMARY KEY CLUSTERED 
(
	Id ASC
));


CREATE TABLE AspNetUserClaims(
	Id int IDENTITY(1,1) NOT NULL,
	ClaimType nvarchar(max) NULL,
	ClaimValue nvarchar(max) NULL,
	UserId nvarchar(450) NOT NULL,
 CONSTRAINT PK_AspNetUserClaims PRIMARY KEY CLUSTERED 
(
	Id ASC
)
);

CREATE TABLE AspNetUserLogins(
	LoginProvider nvarchar(450) NOT NULL,
	ProviderKey nvarchar(450) NOT NULL,
	ProviderDisplayName nvarchar(max) NULL,
	UserId nvarchar(450) NOT NULL,
 CONSTRAINT PK_AspNetUserLogins PRIMARY KEY CLUSTERED 
(
	LoginProvider ASC,
	ProviderKey ASC
)
);

CREATE TABLE AspNetUserRoles(
	UserId nvarchar(450) NOT NULL,
	RoleId nvarchar(450) NOT NULL,
 CONSTRAINT PK_AspNetUserRoles PRIMARY KEY CLUSTERED 
(
	UserId ASC,
	RoleId ASC
)
);

CREATE TABLE AspNetUsers(
	Id nvarchar(450) NOT NULL,
	AccessFailedCount int NOT NULL,
	ConcurrencyStamp nvarchar(max) NULL,
	Email nvarchar(256) NULL,
	EmailConfirmed bit NOT NULL,
	LockoutEnabled bit NOT NULL,
	LockoutEnd datetimeoffset(7) NULL,
	NormalizedEmail nvarchar(256) NULL,
	NormalizedUserName nvarchar(256) NULL,
	PasswordHash nvarchar(max) NULL,
	PhoneNumber nvarchar(max) NULL,
	PhoneNumberConfirmed bit NOT NULL,
	SecurityStamp nvarchar(max) NULL,
	TwoFactorEnabled bit NOT NULL,
	UserName nvarchar(256) NULL,
 CONSTRAINT PK_AspNetUsers PRIMARY KEY CLUSTERED 
(
	Id ASC
)
);

CREATE TABLE AspNetUserTokens(
	UserId nvarchar(450) NOT NULL,
	LoginProvider nvarchar(450) NOT NULL,
	Name nvarchar(450) NOT NULL,
	Value nvarchar(max) NULL,
 CONSTRAINT PK_AspNetUserTokens PRIMARY KEY CLUSTERED 
(
	UserId ASC,
	LoginProvider ASC,
	Name ASC
)
);


/*CREATE PK*/

ALTER TABLE dbo.Banco ADD CONSTRAINT PK_Banco_BancoId PRIMARY KEY CLUSTERED(BancoId asc);
ALTER TABLE dbo.Sucursal ADD CONSTRAINT PK_Sucursal_SucursalId PRIMARY KEY CLUSTERED(SucursalId asc);
ALTER TABLE dbo.OrdenPago ADD CONSTRAINT PK_OrdenPago_OrdenPagolId PRIMARY KEY CLUSTERED(OrdenPagoId asc);
ALTER TABLE dbo.EstadoOrdenPago ADD CONSTRAINT PK_EstadoOrdenPago_EstadoOrdenPagoId PRIMARY KEY CLUSTERED(EstadoOrdenPagoId asc);


/*CREATE FK*/

ALTER TABLE dbo.Sucursal  WITH CHECK ADD  CONSTRAINT FK_Sucursal_Banco_BancoId FOREIGN KEY(BancoId)
REFERENCES dbo.Banco (BancoId)
GO
ALTER TABLE dbo.Sucursal CHECK CONSTRAINT FK_Sucursal_Banco_BancoId
GO


ALTER TABLE dbo.OrdenPago  WITH CHECK ADD  CONSTRAINT FK_OrdenPago_Sucursal_SucursalId FOREIGN KEY(SucursalId)
REFERENCES dbo.Sucursal (SucursalId)
GO
ALTER TABLE dbo.OrdenPago CHECK CONSTRAINT FK_OrdenPago_Sucursal_SucursalId
GO

ALTER TABLE dbo.OrdenPago  WITH CHECK ADD  CONSTRAINT FK_OrdenPago_EstadoOrdenPago_EstadoOrdenPagoId FOREIGN KEY(EstadoOrdenPagoId)
REFERENCES dbo.EstadoOrdenPago (EstadoOrdenPagoId)
GO
ALTER TABLE dbo.OrdenPago CHECK CONSTRAINT FK_OrdenPago_EstadoOrdenPago_EstadoOrdenPagoId
GO

ALTER TABLE dbo.AspNetRoleClaims  WITH CHECK ADD  CONSTRAINT FK_AspNetRoleClaims_AspNetRoles_RoleId FOREIGN KEY(RoleId)
REFERENCES dbo.AspNetRoles (Id)
ON DELETE CASCADE
GO
ALTER TABLE dbo.AspNetRoleClaims CHECK CONSTRAINT FK_AspNetRoleClaims_AspNetRoles_RoleId
GO
ALTER TABLE dbo.AspNetUserClaims  WITH CHECK ADD  CONSTRAINT FK_AspNetUserClaims_AspNetUsers_UserId FOREIGN KEY(UserId)
REFERENCES dbo.AspNetUsers (Id)
ON DELETE CASCADE
GO
ALTER TABLE dbo.AspNetUserClaims CHECK CONSTRAINT FK_AspNetUserClaims_AspNetUsers_UserId
GO
ALTER TABLE dbo.AspNetUserLogins  WITH CHECK ADD  CONSTRAINT FK_AspNetUserLogins_AspNetUsers_UserId FOREIGN KEY(UserId)
REFERENCES dbo.AspNetUsers (Id)
ON DELETE CASCADE
GO
ALTER TABLE dbo.AspNetUserLogins CHECK CONSTRAINT FK_AspNetUserLogins_AspNetUsers_UserId
GO
ALTER TABLE dbo.AspNetUserRoles  WITH CHECK ADD  CONSTRAINT FK_AspNetUserRoles_AspNetRoles_RoleId FOREIGN KEY(RoleId)
REFERENCES dbo.AspNetRoles (Id)
ON DELETE CASCADE
GO
ALTER TABLE dbo.AspNetUserRoles CHECK CONSTRAINT FK_AspNetUserRoles_AspNetRoles_RoleId
GO
ALTER TABLE dbo.AspNetUserRoles  WITH CHECK ADD  CONSTRAINT FK_AspNetUserRoles_AspNetUsers_UserId FOREIGN KEY(UserId)
REFERENCES dbo.AspNetUsers (Id)
ON DELETE CASCADE
GO
ALTER TABLE dbo.AspNetUserRoles CHECK CONSTRAINT FK_AspNetUserRoles_AspNetUsers_UserId
GO

/*Insertar data*/

SET IDENTITY_INSERT dbo.EstadoOrdenPago ON
INSERT INTO dbo.EstadoOrdenPago(EstadoOrdenPagoId,Nombre,CreatedBy) VALUES(1,'Pagado','einca');
INSERT INTO dbo.EstadoOrdenPago(EstadoOrdenPagoId,Nombre,CreatedBy) VALUES(2,'Declinada','einca');
INSERT INTO dbo.EstadoOrdenPago(EstadoOrdenPagoId,Nombre,CreatedBy) VALUES(3,'Fallido','einca');
INSERT INTO dbo.EstadoOrdenPago(EstadoOrdenPagoId,Nombre,CreatedBy) VALUES(4,'Anulado','einca');
SET IDENTITY_INSERT dbo.EstadoOrdenPago OFF


SET IDENTITY_INSERT dbo.Banco ON
INSERT INTO dbo.Banco(BancoId,Nombre,Direccion,CreatedBy) VALUES(1,'Banco Financiero','Calle Basadre N� 310 San Isidro','einca');
INSERT INTO dbo.Banco(BancoId,Nombre,Direccion,CreatedBy) VALUES(2,'Banco el Comercio','Av. Navarrete','einca');
SET IDENTITY_INSERT dbo.Banco OFF


SET IDENTITY_INSERT dbo.Sucursal ON
INSERT INTO dbo.Sucursal(SucursalId,BancoId,Nombre,Direccion,CreatedBy) VALUES(1,1,'Agencia CAPON','Jr. Ucayali n�762','einca');
INSERT INTO dbo.Sucursal(SucursalId,BancoId,Nombre,Direccion,CreatedBy) VALUES(2,1,'Agencia CARSA COMAS','Av. Tupac Amar� 1119 - km. 11','einca');
INSERT INTO dbo.Sucursal(SucursalId,BancoId,Nombre,Direccion,CreatedBy) VALUES(3,1,'Agencia CARSA HUAYCAN','Av. Jos� Carlos Mari�tegui lt.9, zona "b", Huayc�n','einca');
INSERT INTO dbo.Sucursal(SucursalId,BancoId,Nombre,Direccion,CreatedBy) VALUES(4,1,'Agencia CHACARILLA','Calle Monte Rosa n�176. - C.C. Chacarilla','einca');
SET IDENTITY_INSERT dbo.Sucursal OFF
go


SET IDENTITY_INSERT dbo.OrdenPago ON
INSERT INTO dbo.OrdenPago(OrdenPagoId,SucursalId, Monto, Moneda, CreatedBy) VALUES(1,1,2480.25,1,'einca');
INSERT INTO dbo.OrdenPago(OrdenPagoId,SucursalId, Monto, Moneda, CreatedBy) VALUES(2,1,125.28,1,'einca');
SET IDENTITY_INSERT dbo.OrdenPago OFF

INSERT dbo.AspNetUsers (Id, AccessFailedCount, ConcurrencyStamp, Email, EmailConfirmed, LockoutEnabled, LockoutEnd, NormalizedEmail, NormalizedUserName, PasswordHash, PhoneNumber, PhoneNumberConfirmed, SecurityStamp, TwoFactorEnabled, UserName) VALUES (N'206606b7-38e9-4ee6-86d2-8c748f60131d', 0, N'aa3e8d72-52c4-4227-8c78-272991c36721', N'maria@hotmail.com', 0, 1, NULL, N'MARIA@HOTMAIL.COM', N'MARIA@HOTMAIL.COM', N'AQAAAAEAACcQAAAAEG7BWQlySuJkzZ5a+6qvndqWQ9YOIqM9dSP9KQQE+FpGKIv3aQh3Pjkh9UQNCbZj2g==', NULL, 0, N'02b15ef4-120d-41a7-b013-a6dc453715a4', 0, N'maria@hotmail.com')
INSERT dbo.AspNetUsers (Id, AccessFailedCount, ConcurrencyStamp, Email, EmailConfirmed, LockoutEnabled, LockoutEnd, NormalizedEmail, NormalizedUserName, PasswordHash, PhoneNumber, PhoneNumberConfirmed, SecurityStamp, TwoFactorEnabled, UserName) VALUES (N'7a19820f-9425-4bb0-8edc-f6bd580ab141', 0, N'0d7b611e-d2b8-4b51-9103-5f4186688d36', N'carlos@hotmail.com', 0, 1, NULL, N'CARLOS@HOTMAIL.COM', N'CARLOS@HOTMAIL.COM', N'AQAAAAEAACcQAAAAEP7oWtImdw/AWWywAewwgpSrbbh94ljp4F7K/zVJn+VQecYN+1HJCgRlc6KjVy57EA==', NULL, 0, N'74b53b92-4a2f-49cc-834a-a20c856ee48e', 0, N'carlos@hotmail.com')
INSERT dbo.AspNetUsers (Id, AccessFailedCount, ConcurrencyStamp, Email, EmailConfirmed, LockoutEnabled, LockoutEnd, NormalizedEmail, NormalizedUserName, PasswordHash, PhoneNumber, PhoneNumberConfirmed, SecurityStamp, TwoFactorEnabled, UserName) VALUES (N'cb66c24a-eebe-4ba0-af49-f46e3e42394d', 0, N'8d938c2f-6996-45e6-9fc0-fb01fb5254c0', N'jose@hotmail.com', 0, 1, NULL, N'JOSE@HOTMAIL.COM', N'JOSE@HOTMAIL.COM', N'AQAAAAEAACcQAAAAECo2cDkfITP0CMrB9G0jaE560GjZZMqyW0WG/qTbK7EwfnQf8qW4ZZstVKJ5cH67pA==', NULL, 0, N'cee6ed5b-8fa3-48f8-ae4f-907b397f50db', 0, N'jose@hotmail.com')


INSERT dbo.AspNetRoles (Id, ConcurrencyStamp, Name, NormalizedName) VALUES (N'11e9c8db-0718-4b25-bee8-bb5b98158054', N'648b191c-1ae7-48a8-bd62-b7dbe65d9c9b', N'Admin', N'ADMIN')
INSERT dbo.AspNetRoles (Id, ConcurrencyStamp, Name, NormalizedName) VALUES (N'212F48C6-ADC0-4C64-BE64-22E7D0BCDFD1', N'1FDB4310-BA1F-4591-A35C-711A2E179D87', N'Operador2', N'OPERADOR2')
INSERT dbo.AspNetRoles (Id, ConcurrencyStamp, Name, NormalizedName) VALUES (N'66FEB9A5-9066-4DAF-9F53-1E7988C33889', N'6213A2FC-8C3A-443C-AB9B-6AEFC16BC7A2', N'Operador1', N'OPERADOR1')

INSERT dbo.AspNetUserRoles (UserId, RoleId) VALUES (N'206606b7-38e9-4ee6-86d2-8c748f60131d', N'212F48C6-ADC0-4C64-BE64-22E7D0BCDFD1')
INSERT dbo.AspNetUserRoles (UserId, RoleId) VALUES (N'7a19820f-9425-4bb0-8edc-f6bd580ab141', N'66FEB9A5-9066-4DAF-9F53-1E7988C33889')
INSERT dbo.AspNetUserRoles (UserId, RoleId) VALUES (N'cb66c24a-eebe-4ba0-af49-f46e3e42394d', N'11e9c8db-0718-4b25-bee8-bb5b98158054')


/*procedimientos almancedos*/


/***********banco**************/
IF EXISTS(SELECT * FROM sys.procedures WHERE name = 'LISTAR_BANCO')
BEGIN
 DROP PROCEDURE LISTAR_BANCO
END
GO

CREATE PROCEDURE LISTAR_BANCO
AS
BEGIN
SET NOCOUNT ON;
SELECT BancoId,Nombre,Direccion,CreatedBy,CreatedOn FROM dbo.Banco;

END;
go

IF EXISTS(SELECT * FROM sys.procedures WHERE name = 'AGREGAR_BANCO')
BEGIN
 DROP PROCEDURE AGREGAR_BANCO
END
GO

CREATE PROCEDURE AGREGAR_BANCO(
@Nombre VARCHAR(60) = NULL,
@Direccion VARCHAR(256) = NULL,
@Usuario VARCHAR(20) = NULL
)
AS
BEGIN
SET NOCOUNT ON;

INSERT INTO dbo.Banco(Nombre,Direccion,CreatedBy) VALUES(@Nombre,@Direccion,@Usuario);

END;
GO



IF EXISTS(SELECT * FROM sys.procedures WHERE name = 'OBTENER_BANCO')
BEGIN
 DROP PROCEDURE OBTENER_BANCO
END
GO

CREATE PROCEDURE OBTENER_BANCO(
@BancoId INT = 0
)
AS
BEGIN
SET NOCOUNT ON;
SELECT BancoId,Nombre,Direccion,CreatedBy,CreatedOn FROM dbo.Banco WHERE BancoId = @BancoId;

END;
GO

IF EXISTS(SELECT * FROM sys.procedures WHERE name = 'ELIMINAR_BANCO')
BEGIN
 DROP PROCEDURE ELIMINAR_BANCO
END
GO

CREATE PROCEDURE ELIMINAR_BANCO(
@BancoId INT = 0
)
AS
BEGIN
SET NOCOUNT ON;
DELETE FROM dbo.Banco WHERE BancoId = @BancoId;

END;


GO

IF EXISTS(SELECT * FROM sys.procedures WHERE name = 'ACTUALIZAR_BANCO')
BEGIN
 DROP PROCEDURE ACTUALIZAR_BANCO
END
GO

CREATE PROCEDURE ACTUALIZAR_BANCO(
@BancoId INT = 0,
@Nombre VARCHAR(60) = NULL,
@Direccion VARCHAR(256) = NULL,
@Usuario VARCHAR(20) = NULL
)
AS
BEGIN
SET NOCOUNT ON;

UPDATE
   dbo.Banco
SET
  Nombre = ISNULL(@Nombre,Nombre),
  Direccion = ISNULL(@Direccion,Direccion),
  ModifiedBy = @Usuario,
  ModifiedOn = GETDATE()
WHERE
  BancoId = @BancoId;

END;
GO



/***********sucursal**************/
IF EXISTS(SELECT * FROM sys.procedures WHERE name = 'LISTAR_SUCURSAL')
BEGIN
 DROP PROCEDURE LISTAR_SUCURSAL
END
GO

CREATE PROCEDURE LISTAR_SUCURSAL
AS
BEGIN
SET NOCOUNT ON;
SELECT SucursalId,s.Nombre as Sucursal, s.BancoId, b.Nombre as Banco ,s.Direccion,s.CreatedBy,s.CreatedOn FROM dbo.Sucursal as s INNER JOIN dbo.Banco b ON s.BancoId = b.BancoId;

END;
go

IF EXISTS(SELECT * FROM sys.procedures WHERE name = 'LISTAR_SUCURSAL_POR_BANCO')
BEGIN
 DROP PROCEDURE LISTAR_SUCURSAL_POR_BANCO
END
GO

CREATE PROCEDURE LISTAR_SUCURSAL_POR_BANCO(
@BancoId INT = 0
)
AS
BEGIN
SET NOCOUNT ON;
SELECT SucursalId,s.Nombre as Sucursal, s.BancoId, b.Nombre as Banco ,s.Direccion,s.CreatedBy,s.CreatedOn FROM dbo.Sucursal as s INNER JOIN dbo.Banco b ON s.BancoId = b.BancoId
WHERE s.BancoId = @BancoId;

END;
go


IF EXISTS(SELECT * FROM sys.procedures WHERE name = 'AGREGAR_SUCURSAL')
BEGIN
 DROP PROCEDURE AGREGAR_SUCURSAL
END
GO

CREATE PROCEDURE AGREGAR_SUCURSAL(
@BancoId INT = 0,
@Nombre VARCHAR(60) = NULL,
@Direccion VARCHAR(256) = NULL,
@Usuario VARCHAR(20) = NULL
)
AS
BEGIN
SET NOCOUNT ON;

INSERT INTO dbo.Sucursal(BancoId,Nombre,Direccion,CreatedBy) VALUES(@BancoId,@Nombre,@Direccion,@Usuario);

END;
GO



IF EXISTS(SELECT * FROM sys.procedures WHERE name = 'OBTENER_SUCURSAL')
BEGIN
 DROP PROCEDURE OBTENER_SUCURSAL
END
GO

CREATE PROCEDURE OBTENER_SUCURSAL(
@SucursalId INT = 0
)
AS
BEGIN
SET NOCOUNT ON;
SELECT SucursalId,s.Nombre as Sucursal, s.BancoId, b.Nombre as Banco ,s.Direccion,s.CreatedBy,s.CreatedOn FROM dbo.Sucursal as s INNER JOIN dbo.Banco b ON s.BancoId = b.BancoId
WHERE SucursalId = @SucursalId;

END;
GO

IF EXISTS(SELECT * FROM sys.procedures WHERE name = 'ELIMINAR_SUCURSAL')
BEGIN
 DROP PROCEDURE ELIMINAR_SUCURSAL
END
GO

CREATE PROCEDURE ELIMINAR_SUCURSAL(
@SucursalId INT = 0
)
AS
BEGIN
SET NOCOUNT ON;
DELETE FROM dbo.Sucursal WHERE SucursalId = @SucursalId;

END;
GO

IF EXISTS(SELECT * FROM sys.procedures WHERE name = 'ACTUALIZAR_SUCURSAL')
BEGIN
 DROP PROCEDURE ACTUALIZAR_SUCURSAL
END
GO

CREATE PROCEDURE ACTUALIZAR_SUCURSAL(
@BancoId INT = 0,
@SucursalId INT = 0,
@Nombre VARCHAR(60) = NULL,
@Direccion VARCHAR(256) = NULL,
@Usuario VARCHAR(20) = NULL
)
AS
BEGIN
SET NOCOUNT ON;

UPDATE
   dbo.Sucursal
SET
  Nombre = ISNULL(@Nombre,Nombre),
  Direccion = ISNULL(@Direccion,Direccion),
  ModifiedBy = @Usuario,
  ModifiedOn = GETDATE()
WHERE
  SucursalId = @SucursalId;

END;
GO



/***********Orden pago**************/
IF EXISTS(SELECT * FROM sys.procedures WHERE name = 'LISTAR_ORDENPAGO')
BEGIN
 DROP PROCEDURE LISTAR_ORDENPAGO
END
GO

CREATE PROCEDURE LISTAR_ORDENPAGO
AS
BEGIN
SET NOCOUNT ON;
SELECT 
   op.OrdenPagoId,
   op.FechaPago,
   op.Monto,
   op.Moneda as MonedaId,
   case op.Moneda WHEN 1 THEN 'SOLES' ELSE 'D�LARES' END AS Moneda,
   op.SucursalId,
   s.Nombre as Sucursal, 
   s.BancoId,
   s.Direccion as DireccionSucursal,
   b.Nombre as Banco,
   op.EstadoOrdenPagoId,
   eop.Nombre as EstadoOrdenPago,
   s.Direccion,s.CreatedBy,
   s.CreatedOn
FROM 
   dbo.OrdenPago as op INNER JOIN dbo.Sucursal s ON op.SucursalId = s.SucursalId
   INNER JOIN dbo.Banco b on s.BancoId = b.BancoId
   INNER JOIN dbo.EstadoOrdenPago as eop on eop.EstadoOrdenPagoId = op.EstadoOrdenPagoId;

END;
go



IF EXISTS(SELECT * FROM sys.procedures WHERE name = 'AGREGAR_ORDENPAGO')
BEGIN
 DROP PROCEDURE AGREGAR_ORDENPAGO
END
GO

CREATE PROCEDURE AGREGAR_ORDENPAGO(
@SucursalId INT  = 0,
@MonedaId INT = 0,
@EstadoOrdenPagoId INT = 0,
@Monto NUMERIC(18,2) = 0.00,
@FechaPago DATETIME = NULL,
@Usuario VARCHAR(20) = NULL
)
AS
BEGIN
SET NOCOUNT ON;

INSERT INTO dbo.OrdenPago(SucursalId,Monto, Moneda,EstadoOrdenPagoId,FechaPago, CreatedBy ) VALUES(@SucursalId,@Monto,@MonedaId,@EstadoOrdenPagoId,@FechaPago,@Usuario);

END;
GO



IF EXISTS(SELECT * FROM sys.procedures WHERE name = 'OBTENER_ORDENPAGO')
BEGIN
 DROP PROCEDURE OBTENER_ORDENPAGO
END
GO

CREATE PROCEDURE OBTENER_ORDENPAGO(
@OrdePagoId INT = 0
)
AS
BEGIN
SET NOCOUNT ON;
SELECT 
   op.OrdenPagoId,
   op.FechaPago,
   op.Monto,
   op.Moneda as MonedaId,
   case op.Moneda WHEN 1 THEN 'SOLES' ELSE 'D�LARES' END AS Moneda,
   op.SucursalId,
   s.Nombre as Sucursal, 
   s.BancoId,
   s.Direccion as DireccionSucursal,
   b.Nombre as Banco,
   op.EstadoOrdenPagoId,
   eop.Nombre as EstadoOrdenPago,
   s.Direccion,s.CreatedBy,
   s.CreatedOn
FROM 
   dbo.OrdenPago as op INNER JOIN dbo.Sucursal s ON op.SucursalId = s.SucursalId
   INNER JOIN dbo.Banco b on s.BancoId = b.BancoId
   INNER JOIN dbo.EstadoOrdenPago as eop on eop.EstadoOrdenPagoId = op.EstadoOrdenPagoId
WHERE 
   OrdenPagoId  =  @OrdePagoId;

END;
GO

IF EXISTS(SELECT * FROM sys.procedures WHERE name = 'ELIMINAR_ORDENPAGO')
BEGIN
 DROP PROCEDURE ELIMINAR_ORDENPAGO
END
GO

CREATE PROCEDURE ELIMINAR_ORDENPAGO(
@OrdenPagoId INT = 0
)
AS
BEGIN
SET NOCOUNT ON;
DELETE FROM dbo.OrdenPago WHERE OrdenPagoId = @OrdenPagoId;

END;
GO

IF EXISTS(SELECT * FROM sys.procedures WHERE name = 'ACTUALIZAR_ORDENPAGO')
BEGIN
 DROP PROCEDURE ACTUALIZAR_ORDENPAGO
END
GO

CREATE PROCEDURE ACTUALIZAR_ORDENPAGO(
@OrdenPagoId INT = 0,
@Monto NUMERIC(18,2) = NULL,
@EstadoOrdenPagoId INT = NULL,
@FechaPago DATETIME = NULL,
@Usuario VARCHAR(20) = NULL
)
AS
BEGIN
SET NOCOUNT ON;

UPDATE
   dbo.OrdenPago
SET
  Monto = ISNULL(@Monto,Monto),
  EstadoOrdenPagoId = ISNULL(@EstadoOrdenPagoId,EstadoOrdenPagoId),
  FechaPago = ISNULL( @FechaPago,FechaPago),
  ModifiedBy = @Usuario,
  ModifiedOn = GETDATE()
WHERE
  OrdenPagoId = @OrdenPagoId;

END;
GO

IF EXISTS(SELECT * FROM sys.procedures WHERE name = 'LISTAR_ORDENPAGO_POR_SUCURSAL')
BEGIN
 DROP PROCEDURE LISTAR_ORDENPAGO_POR_SUCURSAL
END
GO

CREATE PROCEDURE LISTAR_ORDENPAGO_POR_SUCURSAL(
@SucursalId INT = 0,
@MonedaId INT = 0
)
AS
BEGIN
SET NOCOUNT ON;
SELECT 
   op.OrdenPagoId,
   op.FechaPago,
   op.Monto,
   op.Moneda as MonedaId,
   case op.Moneda WHEN 1 THEN 'SOLES' ELSE 'D�LARES' END AS Moneda,
   op.SucursalId,
   s.Nombre as Sucursal, 
   s.BancoId,
   s.Direccion as DireccionSucursal,
   b.Nombre as Banco,
   op.EstadoOrdenPagoId,
   eop.Nombre as EstadoOrdenPago,
   s.Direccion,s.CreatedBy,
   s.CreatedOn
FROM 
   dbo.OrdenPago as op INNER JOIN dbo.Sucursal s ON op.SucursalId = s.SucursalId
   INNER JOIN dbo.Banco b on s.BancoId = b.BancoId
   INNER JOIN dbo.EstadoOrdenPago as eop on eop.EstadoOrdenPagoId = op.EstadoOrdenPagoId
WHERE
   s.SucursalId = @SucursalId
   and op.Moneda = @MonedaId;
END;
go



