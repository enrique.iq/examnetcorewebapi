﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App.API.Models
{
    public class EstadoOrdenPago : EntityBase
    {

        public EstadoOrdenPago()
        {
            OrdenPago = new HashSet<OrdenPago>();
        }
        public int? EstadoOrdenPagoId { get; set; }

        public string Nombre { get; set; }
        public virtual ICollection<OrdenPago> OrdenPago { get; set; }
    }
}
