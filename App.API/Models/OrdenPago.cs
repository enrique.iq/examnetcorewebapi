﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App.API.Models
{
    public class OrdenPago:EntityBase
    {
        public int? OrdenPagoId { get; set; }
        public int? SucursalId { get; set; }      
        public decimal? Monto { get; set; }
        public int? MonedaId { get; set; }
        public int? EstadoOrdenPagoId { get; set; }
        public DateTime? FechaPago { get; set; }
        public virtual EstadoOrdenPago EstadoOrdenPago { get; set; }
        public virtual Sucursal Sucursal { get; set; }
        public virtual Moneda Moneda { get; set; }
    }
}
