﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App.API.Models
{
    public class Sucursal:EntityBase
    {
        public Sucursal()
        {
            OrdenPago = new HashSet<OrdenPago>();
        }
        public int? SucursalId { get; set; }
        public int? BancoId { get; set; }     
        public string Nombre { get; set; }       
        public string Direccion { get; set; }
        public virtual Banco Banco { get; set; }        
        public virtual ICollection<OrdenPago> OrdenPago { get; set; }

    }
}
