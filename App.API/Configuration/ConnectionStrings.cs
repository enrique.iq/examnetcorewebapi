﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App.API.Configuration
{
    public class ConnectionStrings
    {
        public string DbConnection { get; set; }
    }
}
