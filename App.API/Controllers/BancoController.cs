﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using App.API.DataProvider;
using App.API.Models;


namespace App.API.Controllers
{
    [Route("api/banco")]
    public class BancoController : Controller
    {
        private IBancoDAO providerDAO;      
        public BancoController(IBancoDAO providerDAO)
        {
            this.providerDAO = providerDAO;
        }

        [HttpGet]
        public async Task<IEnumerable<Banco>> Get()
        {

            return await this.providerDAO.Get();
        }

        [HttpGet("{id}")]
        public async Task<Banco> Get(int id)
        {
            return await this.providerDAO.GetById(id);
        }

        [HttpPost]
        public async Task Post([FromBody]Banco banco)
        {
            await this.providerDAO.Add(banco);
        }

        [HttpPut("{id}")]
        public async Task Put(int id, [FromBody]Banco banco)
        {
            await this.providerDAO.Update(banco);
        }

        [HttpDelete("{id}")]
        public async Task Delete(int id)
        {
            await this.providerDAO.Delete(id);
        }
    }
}
