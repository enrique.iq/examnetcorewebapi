﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using App.API.DataProvider;
using App.API.Models;

namespace App.API.Controllers
{
    [Route("api/ordenpago")]
    public class OrdenPagoController : Controller
    {
        private IOrdenPagoDAO proveedorDAO;
        public OrdenPagoController(IOrdenPagoDAO proveedorDAO)
        {
            this.proveedorDAO = proveedorDAO;
        }
        [HttpGet]
        public async Task<IEnumerable<OrdenPago>> Get()
        {
            return await this.proveedorDAO.Get();
        }
        [HttpGet("{id}")]
        public async Task<OrdenPago> Get(int id)
        {
            return await this.proveedorDAO.GetById(id);
        }
        [HttpPost]
        public async Task Post([FromBody]OrdenPago obj)
        {
            await this.proveedorDAO.Add(obj);
        }
        [HttpPut("{id}")]
        public async Task Put(int id, [FromBody]OrdenPago obj)
        {
            await this.proveedorDAO.Update(obj);
        }
        [HttpDelete("{id}")]
        public async Task Delete(int id)
        {
            await this.proveedorDAO.Delete(id);
        }

        [HttpGet]
        [Route("sucursal/{SucursalId}/{MonedaId}")]
        public async Task<IEnumerable<OrdenPago>> GetOrdenPagoBySucursal(int SucursalId, string monedaId)
        {
            return await this.proveedorDAO.GetOrdenPagoBySucursal(SucursalId, monedaId);
        }
    }
}
